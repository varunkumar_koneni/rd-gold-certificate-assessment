package com.epam;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.anyString;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.dao.DataIntegrityViolationException;

import com.epam.dto.AssociateDTO;
import com.epam.exception.AssociateException;
import com.epam.model.Associate;
import com.epam.model.Batch;
import com.epam.repository.AssociateRepositary;
import com.epam.repository.BatchRepository;
import com.epam.service.AssociateService;

@ExtendWith(MockitoExtension.class)
class TestAssociateService {
	
	@Mock
	AssociateRepositary associateRepositary;
	
	@InjectMocks
	AssociateService associateService;
	
	@Mock
	BatchRepository batchRepository;
	
    @Mock
    ModelMapper mapper;
    
    
    @Test
    void testDeleteAssociate() throws AssociateException  {
        int id = 1;
        Associate associate = new Associate();
        associate.setId(id);
        Optional<Associate> optionalAssociate = Optional.of(associate);        
        when(associateRepositary.findById(id)).thenReturn(optionalAssociate);
        doNothing().when(associateRepositary).deleteById(id);      
        associateService.deleteAssociate(id);       
        verify(associateRepositary, times(1)).deleteById(id);

   }
    
    
    @Test
    void testDeleteAssociateThrowsAssociateException() {
        int id = 1;
        Optional<Associate> optionalAssociate = Optional.empty();      
        when(associateRepositary.findById(id)).thenReturn(optionalAssociate);       
        assertThrows(AssociateException.class, () -> associateService.deleteAssociate(id));
    }
    
    

	@Test
	 void testCreateAssociate() throws AssociateException {
		Batch batch = new Batch();
		batch.setName("java");
		batch.setPractice("java");
		batch.setId(1);
		batch.setStartDate(new Date());
		batch.setEndDate(new Date());
		
		AssociateDTO associateDTO = new AssociateDTO();
		associateDTO.setName("varun");
		associateDTO.setCollege("vignan");
		associateDTO.setEmail("konenivarun");
		associateDTO.setGender("male");
		associateDTO.setId(1);
		associateDTO.setStatus("Alive");
		associateDTO.setBatch(batch);

		Associate associate = new Associate();
		associate.setName("varun");
		associate.setCollege("vignan");
		associate.setEmail("konenivarun");
		associate.setGender("male");
		associate.setId(1);
		associate.setStatus("Alive");
		associate.setBatch(batch);

		when(mapper.map(associateDTO, Associate.class)).thenReturn(associate);
		when(mapper.map(associate, AssociateDTO.class)).thenReturn(associateDTO);

		when(associateRepositary.save(Mockito.any(Associate.class))).thenReturn(associate);

		AssociateDTO result = associateService.createAssociate(associateDTO);

		assertEquals(result, associateDTO);
	}
	
	@Test
	 void testCreateAssociateFails() throws AssociateException {
		Batch batch = new Batch();
		AssociateDTO associateDTO = new AssociateDTO();
		associateDTO.setName("varun");
		associateDTO.setCollege("vignan");
		associateDTO.setEmail("konenivarun");
		associateDTO.setGender("male");
		associateDTO.setId(1);
		associateDTO.setStatus("Alive");
		associateDTO.setBatch(batch);

		Associate associate = new Associate();
		associate.setName("varun");
		associate.setCollege("vignan");
		associate.setEmail("konenivarun");
		associate.setGender("male");
		associate.setId(1);
		associate.setStatus("Alive");
		associate.setBatch(batch);

		assertThrows(AssociateException.class, () -> associateService.createAssociate(associateDTO));

	}
	
	
	@Test
	void testModifyAssociate() throws AssociateException {
		Batch batch = new Batch();
		AssociateDTO associateDTO = new AssociateDTO();
		associateDTO.setName("varun");
		associateDTO.setCollege("vignan");
		associateDTO.setEmail("konenivarun");
		associateDTO.setGender("male");
		associateDTO.setId(1);
		associateDTO.setStatus("Alive");
		associateDTO.setBatch(batch);

		Associate associate = new Associate();
		associate.setName("varun");
		associate.setCollege("vignan");
		associate.setEmail("konenivarun");
		associate.setGender("male");
		associate.setId(1);
		associate.setStatus("Alive");
		associate.setBatch(batch);

		when(mapper.map(associateDTO, Associate.class)).thenReturn(associate);
		when(mapper.map(associate, AssociateDTO.class)).thenReturn(associateDTO);

		when(associateRepositary.save(Mockito.any(Associate.class))).thenReturn(associate);

		AssociateDTO result = associateService.modifyAssociate(associateDTO);

		assertEquals(result, associateDTO);
	}
	
	@Test
	void testModifyAssociateFails() throws AssociateException {
		String gender="male";
		Batch batch = new Batch();
		AssociateDTO associateDTO = new AssociateDTO();
		associateDTO.setName("varun");
		associateDTO.setCollege("vignan");
		associateDTO.setEmail("konenivarun");
		associateDTO.setGender("male");
		associateDTO.setId(1);
		associateDTO.setStatus("Alive");
		associateDTO.setBatch(batch);

		Associate associate = new Associate();
		associate.setName("varun");
		associate.setCollege("vignan");
		associate.setEmail("konenivarun");
		associate.setGender("male");
		associate.setId(1);
		associate.setStatus("Alive");
		associate.setBatch(batch);

		assertThrows(AssociateException.class, () -> associateService.modifyAssociate(associateDTO));

	}
	
	
	@Test
	void testGetAssociatesByGender() throws AssociateException {
		String gender="male";
		Batch batch=new Batch();
	    AssociateDTO associateDTO = new AssociateDTO();
	    associateDTO.setName("varun");
	    associateDTO.setCollege("vignan");
	    associateDTO.setEmail("konenivarun");
	    associateDTO.setGender("male");
	    associateDTO.setId(1);
	    associateDTO.setStatus("Alive");
	    associateDTO.setBatch(batch);

	    Associate associate = new Associate();
	    associate.setName("varun");
	    associate.setCollege("vignan");
	    associate.setEmail("konenivarun");
	    associate.setGender("male");
	    associate.setId(1);
	    associate.setStatus("Alive");
	    associate.setBatch(batch);

	    List<Associate> associates = new ArrayList<>();
	    associates.add(associate);
	    when(associateRepositary.findAssociatesByGender(gender)).thenReturn(associates);
	    when(mapper.map(associate, AssociateDTO.class)).thenReturn(associateDTO);

	    associateService.getAssociatesByGender(gender);

	    verify(associateRepositary).findAssociatesByGender(gender);
	}  
	
    @Test
     void testGetAssociatesByGender_NoAssociatesPresent_ThrowsAssociateException() {
       String gender = "male";
        when(associateRepositary.findAssociatesByGender(anyString())).thenReturn(null);

        assertThrows(AssociateException.class, () -> associateService.getAssociatesByGender(gender));
    }


}
