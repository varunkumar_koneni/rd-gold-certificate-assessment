package com.epam;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.controller.AssociateController;
import com.epam.dto.AssociateDTO;
import com.epam.exception.AssociateException;
import com.epam.service.AssociateService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(AssociateController.class)
class TestAssociateController {
	
	@Autowired
	MockMvc mockMvc;
	
	@Autowired
	ModelMapper mapper;
	
	@MockBean
	AssociateService associateService;
	
	
	@Test
	void testCreateAssociate() throws Exception {
		AssociateDTO associateDTO = new AssociateDTO();
		associateDTO.setName("varun");
		associateDTO.setCollege("vignan");
		associateDTO.setEmail("konenivarun@gmail.com");
		associateDTO.setGender("male");
		associateDTO.setId(1);
		associateDTO.setStatus("Alive");	

		when(associateService.createAssociate(Mockito.any())).thenReturn(associateDTO);

		mockMvc.perform(post("/rd/associates").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associateDTO))).andExpect(status().isCreated());
	}
	
	@Test
	void testCreateAssociateFails() throws Exception {
		AssociateDTO associateDTO = new AssociateDTO();
		associateDTO.setName("varun");
		associateDTO.setCollege("vignan");
		associateDTO.setEmail("konenivarun");
		associateDTO.setId(1);
		associateDTO.setStatus("Alive");	

		when(associateService.createAssociate(Mockito.any())).thenReturn(associateDTO);

		mockMvc.perform(post("/rd/associates").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associateDTO))).andExpect(status().isBadRequest());
	}
	
	@Test
	void testUpdateAssociate() throws Exception {
		AssociateDTO associateDTO = new AssociateDTO();
		associateDTO.setName("varun");
		associateDTO.setCollege("vignan");
		associateDTO.setEmail("konenivarun@gmail.com");
		associateDTO.setGender("male");
		associateDTO.setId(1);
		associateDTO.setStatus("Alive");	

		when(associateService.modifyAssociate(Mockito.any())).thenReturn(associateDTO);

		mockMvc.perform(put("/rd/associates").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associateDTO))).andExpect(status().isOk());
	}
	
	
	@Test
	void testDeleteAssociate() throws Exception {
		Mockito.doNothing().when(associateService).deleteAssociate(1);
		mockMvc.perform(delete("/rd/associates/{id}", 1)).andExpect(status().isNoContent());
	}
	
	@Test
	void testGetAssociatesByGender() throws Exception {
		
		List<AssociateDTO> associates = new ArrayList<>();

		when(associateService.getAssociatesByGender("male")).thenReturn(associates);
		mockMvc.perform(get("/rd/associates/{gender}", "male")).andExpect(status().isOk());

		
	}
	
	@Test
	void testCreateAssociateThrowsAssociateException() throws Exception {
		AssociateDTO associateDTO = new AssociateDTO();
		associateDTO.setName("varun");
		associateDTO.setCollege("vignan");
		associateDTO.setEmail("konenivarun@gmail.com");
		associateDTO.setGender("male");
		associateDTO.setId(1);
		associateDTO.setStatus("Alive");	

		when(associateService.createAssociate(Mockito.any())).thenThrow(new AssociateException("ExceptionOccured"));

		mockMvc.perform(post("/rd/associates").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associateDTO))).andExpect(status().isBadRequest());
	}
	
	
	
	
	
	
	

}
