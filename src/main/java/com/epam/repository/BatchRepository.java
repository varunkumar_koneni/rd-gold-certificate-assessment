package com.epam.repository;

import org.springframework.stereotype.Repository;

import com.epam.model.Batch;

import org.springframework.data.jpa.repository.JpaRepository;


@Repository
public interface BatchRepository extends JpaRepository<Batch,Integer>{

}
