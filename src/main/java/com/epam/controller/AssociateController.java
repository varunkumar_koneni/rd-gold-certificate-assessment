package com.epam.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.epam.dto.AssociateDTO;
import com.epam.exception.AssociateException;
import com.epam.service.AssociateService;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class AssociateController {
	
	
	@Autowired
	private AssociateService associateService;
	
	@PostMapping("/rd/associates")
	public ResponseEntity<AssociateDTO> createAssociate(@RequestBody @Valid AssociateDTO associateDTO) throws AssociateException {
		log.info("Creating a new associate: {}", associateDTO);
		AssociateDTO createdAssociate = associateService.createAssociate(associateDTO);
		log.info("Associate created successfully: {}", createdAssociate);
		return new ResponseEntity<>(createdAssociate, HttpStatus.CREATED);
	}
	
	@GetMapping("/rd/associates/{gender}")
	public ResponseEntity<List<AssociateDTO>> getAssociateByGender(@PathVariable String gender) throws AssociateException {
		log.info("Fetching associates by gender: {}", gender);
		List<AssociateDTO> associates = associateService.getAssociatesByGender(gender);
		log.info("Fetched {} associates by gender: {}", associates.size(), gender);
		return new ResponseEntity<>(associates, HttpStatus.OK);
	}
	
	@PutMapping("/rd/associates")
	public ResponseEntity<AssociateDTO> modifyAssociate(@RequestBody @Valid AssociateDTO associateDTO) throws AssociateException {
		log.info("Modifying associate: {}", associateDTO);
		AssociateDTO modifiedAssociate = associateService.modifyAssociate(associateDTO);
		log.info("Associate modified successfully: {}", modifiedAssociate);
		return new ResponseEntity<>(modifiedAssociate, HttpStatus.OK);
	}
	
	@DeleteMapping("/rd/associates/{id}")
	public ResponseEntity<Void> deleteAssociate(@PathVariable int id) throws AssociateException {
		log.info("Deleting associate with ID: {}", id);
		associateService.deleteAssociate(id);
		log.info("Associate with ID {} deleted successfully", id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
