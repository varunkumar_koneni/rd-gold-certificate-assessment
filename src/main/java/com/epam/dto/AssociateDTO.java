package com.epam.dto;

import com.epam.model.Batch;
import jakarta.validation.constraints.Email;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;


public class AssociateDTO {
	
	int id;
	@NotBlank(message = "The name should not be empty")
	private String name;
	@NotBlank(message = "The mail should not be blank")
	@Email(message = "The mail should be proper format")
	private String email;
	@NotBlank(message = "The gender should not be blank")
	private String gender;
	@NotBlank(message = "The college name should not be empty")
	private String college;
	@NotBlank(message = "The status should not be empty")
	private String status;
	
	private Batch batch;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCollege() {
		return college;
	}

	public void setCollege(String college) {
		this.college = college;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Batch getBatch() {
		return batch;
	}

	public void setBatch(Batch batch) {
		this.batch = batch;
	}
	
	
}
