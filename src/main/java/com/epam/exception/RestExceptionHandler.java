package com.epam.exception;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import com.epam.controller.AssociateController;
import com.epam.model.ExceptionResponse;

import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Slf4j
public class RestExceptionHandler {
	private static final Logger log = LoggerFactory.getLogger(AssociateController.class);

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<Object> handleIfValuesNull(MethodArgumentNotValidException ex, WebRequest request) {
		List<String> errors = new ArrayList<>();
		ex.getBindingResult().getAllErrors().forEach(error -> {
			String errorMessage = error.getDefaultMessage();
			errors.add(errorMessage);
		});
	    log.error("MethodArgumentNotValidException occured : {}", ex.getMessage());
		ExceptionResponse exceptionResponse = new ExceptionResponse(new Date().toString(),
				HttpStatus.BAD_REQUEST.name(), errors.toString(), request.getDescription(false));
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exceptionResponse);
	}

	@ExceptionHandler(AssociateException.class)
	public ResponseEntity<Object> handleAssociateException(AssociateException ex, WebRequest request) {
	    log.error("An {} occeted : {}", ex.getClass().getSimpleName(), ex.getMessage());
	    ExceptionResponse exceptionResponse = new ExceptionResponse(new Date().toString(),
	    		HttpStatus.BAD_REQUEST.name(), ex.getMessage(), request.getDescription(false));
	    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exceptionResponse);
	}
	
	@ExceptionHandler(DataIntegrityViolationException.class)
	public ResponseEntity<ExceptionResponse> handleDataIntegrityException(DataIntegrityViolationException ex, WebRequest request) {
	    log.error("Data integrity violation exception occurred: {}", ex.getMessage());
	    ExceptionResponse exceptionResponse = new ExceptionResponse(new Date().toString(),
	    		HttpStatus.CONFLICT.name(), "Could not perform the operation, Associate with given mail already exist", request.getDescription(false));    
	    return new ResponseEntity<>(exceptionResponse,HttpStatus.CONFLICT);
	}
	
	

}
