package com.epam.exception;

public class AssociateException  extends Exception{
	
	public AssociateException(String message) {
		super(message);
	}

}
