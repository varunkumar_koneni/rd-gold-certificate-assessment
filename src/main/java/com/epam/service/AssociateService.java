package com.epam.service;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.controller.AssociateController;
import com.epam.dto.AssociateDTO;
import com.epam.exception.AssociateException;
import com.epam.model.Associate;
import com.epam.repository.AssociateRepositary;
import com.epam.repository.BatchRepository;

import lombok.extern.slf4j.Slf4j;

@Service

public class AssociateService {
	
	private static final Logger log = LoggerFactory.getLogger(AssociateController.class);

	
	@Autowired
	AssociateRepositary associateRepositary;
	
	@Autowired
	BatchRepository batchRepository;
	
	@Autowired
	ModelMapper mapper;
	
	public AssociateDTO createAssociate(AssociateDTO associateDTO) throws AssociateException {
        log.info("Creating Associate: {}", associateDTO);
		batchRepository.save(associateDTO.getBatch());
		return Optional.ofNullable(associateRepositary.save(mapper.map(associateDTO, Associate.class)))
				.map(associate->mapper.map(associate, AssociateDTO.class))
				.orElseThrow(()->new AssociateException("Unable to add Associate"));
		
	}

	public List<AssociateDTO> getAssociatesByGender(String gender) throws AssociateException {
        log.info("Getting Associates by gender: {}", gender);
	    return Optional.ofNullable(associateRepositary.findAssociatesByGender(gender))
	            .filter(associates -> !associates.isEmpty())
	            .map(associates -> associates.stream()
	                    .map(associate -> {
	                    	return mapper.map(associate, AssociateDTO.class);
	                    })
	                    .toList())
	            .orElseThrow(() -> new AssociateException("No Associates with given gender present in the database"));
	    
	}

	public AssociateDTO modifyAssociate(AssociateDTO associateDTO) throws AssociateException {
        log.info("Modifying Associate: {}", associateDTO);
		batchRepository.save(associateDTO.getBatch());
		return Optional.ofNullable(associateRepositary.save(mapper.map(associateDTO, Associate.class)))
				.map(associate->mapper.map(associate, AssociateDTO.class))
				.orElseThrow(()->new AssociateException("Unable to update Associate"));
	}

	public void deleteAssociate(int id) throws AssociateException {
        log.info("Deleting Associate with ID: {}", id);
	    Optional<Associate> optionalAssociate = associateRepositary.findById(id);
	    if (optionalAssociate.isPresent()) {
	    	associateRepositary.deleteById(id);
            log.info("Associate deleted successfully");

	    } else {
	        throw new AssociateException("Associate with ID " + id + " not found");
	    }
	}

}
